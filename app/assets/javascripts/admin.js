$(document).ready(function() {
    
    var breeds = $('#pet_breed_id').html();
    $('#pet_species_id').on('change', function(){
        var selectedSpecies = $('#pet_species_id :selected').text();
        var optgroup = "optgroup[label='"+ selectedSpecies + "']";
        var options = $(breeds).filter(optgroup).html();
        $('#pet_breed_id').html(options);
    });

    $('#pet_species_id').trigger('change');

    // define DataTable grid and bind to 'example' static HTML table
    var tbl = $('.datatable').DataTable();

    // Reload on record filter radio button clicks
    $(document).on("click", "#tbl-filters", function() {
        tbl.draw();
    });

    var toggleActive = function(activate) {
        // de-activate any existing selection
        $('#tbl-filters').find('.btn-primary').each(function(index, element) {
            $(element).closest('li').removeClass('active');
        });
        activate.closest('li').addClass('active');
    }

    $.fn.dataTable.ext.search.push(
        function(settings, data, dataIndex) {
            var adopted = $('#tbl-filter-adopted')[0].checked;
            var for_adoption = $('#tbl-filter-for-adoption')[0].checked;
            var all = $('#tbl-filter-all')[0].checked;
            var condition = String(data[data.length-3]); // check condition matched/unmatched column

            if (all) {
                toggleActive($('#tbl-filter-all'));
                return true;
            } else if (adopted) {
                toggleActive($('#tbl-filter-adopted'));
                return ('Yes' === condition);
            } else if (for_adoption) {
                toggleActive($('#tbl-filter-for-adoption'));
                return ('No' === condition);
            }
            return false;
        }
    );
});