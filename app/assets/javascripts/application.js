// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require settings
//= require turbolinks
//= require plugins/bootstrap/bootstrap.min
//= require plugins/icheck/icheck.min
//= require plugins/mcustomscrollbar/jquery.mCustomScrollbar.min
//= require plugins/datatables/jquery.dataTables.min
//= require plugins/bootstrap/bootstrap-datepicker
//= require plugins
//= require actions
//= require admin
//= require cable
//= require channels/activity
