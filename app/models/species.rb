class Species < ApplicationRecord
  #include ActiveModel::Serializers::JSON

  has_many :breeds
  validates_presence_of :name
  # attr_accessor :name
  #
  # def attributes
  #   {'name' => nil}
  # end

  def to_s
    name
  end
end
