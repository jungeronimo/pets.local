class Location < ApplicationRecord
  belongs_to :mappable, polymorphic: true, autosave: true

  geocoded_by :full_address
  after_validation :geocode, if: ->(obj){ obj.address.present? and obj.address_changed? }

  def full_address
    ([address, area, region].reject &:blank?).join(', ')
  end

  def to_s
    full_address
  end
end
