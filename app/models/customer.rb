class Customer < ApplicationRecord
  # Include default devise modules. Others available are:
  #  :lockable, :timeoutable and :omniauthable, :confirmable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :preferences, :class_name => 'CustomerPreference', :autosave => true, :inverse_of => :customer
  has_many :adoptions
  has_many :pets
  has_one :location, as: :mappable, inverse_of: :mappable

  accepts_nested_attributes_for :location, reject_if: :all_blank
  accepts_nested_attributes_for :preferences, reject_if: :all_blank

  def adopt(pet)
    pet.transaction do
      pet.owner = self
      pet.save!
      adoption = self.adoptions.build(pet: pet)
      adoption.save!
      adoption
    end
  end

  def matches
    unless self.pets.count > 0
    Pet.find_by_sql [
        'SELECT p.*
         FROM pets p
         INNER JOIN customer_preferences cp
            ON (cp.species_id IS NULL
                  OR (cp.species_id = p.species_id AND (cp.breed_id IS NULL OR cp.breed_id = p.breed_id)))
         INNER JOIN customers c
            ON cp.customer_id = c.id
         WHERE
            c.id = ?
            AND p.customer_id = 0
            AND (cp.min_age IS NULL OR cp.min_age <= p.age)
            AND (cp.max_age IS NULL OR cp.max_age >= p.age)
            AND p.available_from < ?
         GROUP BY p.id
         ', self.id, Time.now]
    else
      []
    end
  end

  def to_s
    name
  end
end
