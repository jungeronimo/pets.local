class Pet < ApplicationRecord
  belongs_to :breed
  belongs_to :species
  belongs_to :owner, class_name: 'Customer', foreign_key: :customer_id, optional: true
  has_many :adoptions
  has_many :events, as: :observable
  has_one :location, as: :mappable, inverse_of: :mappable

  after_create_commit { create_event }

  accepts_nested_attributes_for :location, reject_if: :all_blank

  validates_presence_of :name

  def adopted?
    self.owner.is_a?(Customer)
  end

  def matches
    unless self.adopted?
      Customer.find_by_sql(
          [
            'SELECT c.*, p2.customer_id
             FROM customers c
             INNER JOIN customer_preferences cp
                ON (cp.customer_id = c.id)
             INNER JOIN pets p
                ON (cp.species_id IS NULL
                      OR (cp.species_id = p.species_id AND (cp.breed_id IS NULL OR cp.breed_id = p.breed_id)))
             LEFT OUTER JOIN pets p2
                ON c.id = p2.customer_id
             WHERE
                p2.customer_id IS NULL
                AND (cp.min_age IS NULL OR cp.min_age <= ?)
                AND (cp.max_age IS NULL OR cp.max_age >= ?)
                AND p.available_from < ?
             GROUP BY c.id
             ',
              self.age,
              self.age,
              Time.now
          ]
      )
    else
      []
    end
  end

  def to_s
    name
  end

  private

  def create_event
    self.events.create message: 'A new pet named ' + self.name +  ' matching your preferences is now available.'
  end

end
