class Event < ApplicationRecord
  belongs_to :observable, polymorphic: true

  after_create_commit { EventBroadcastJob.perform_later self }

end
