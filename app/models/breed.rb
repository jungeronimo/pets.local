class Breed < ApplicationRecord
  belongs_to :species

  validates_presence_of :name

  def to_s
    name
  end
end
