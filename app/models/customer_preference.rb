class CustomerPreference < ApplicationRecord
  belongs_to :customer, :inverse_of => :preferences
  belongs_to :species, optional: true
  belongs_to :breed, optional: true

end
