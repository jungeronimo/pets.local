class BaseController < ApplicationController
  layout 'application'

  before_filter :authorized?

  private
  def authorized?
    unless customer_signed_in?
      flash[:error] = "You must be logged in to view this page."
      redirect_to new_customer_session_path
    end
  end
end
