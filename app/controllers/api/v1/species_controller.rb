module API
  module V1
    class SpeciesController < BaseController

      # GET /species
      def index
        @species = ::Species.includes(:breeds).all
        respond_with(@species)
      end
    end
  end
end
