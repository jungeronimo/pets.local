module API
  module V1
    class CustomerMatchesController < BaseController
      before_action :set_pet, only: [:index]

      # Get /
      def index
        @customers = @pet.matches
        respond_with(@customers)
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_pet
        @pet = ::Pet.find(params[:id])
      end

    end
  end
end
