module API
  module V1
    class CustomersController < BaseController
      before_action :set_customer, only: [:show]

      # POST /pets
      def create
        @customer = ::Customer.new(customer_params)

        if @customer.save
          respond_with(@customer, :status => :created, :location => api_v1_customer_path(@customer))
        else
          respond_with(@customer, status: :unprocessable_entity, template: 'shared/error')
        end
      end

      def show
        respond_with(@customer)
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_customer
        @customer = ::Customer.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def customer_params
        params.require(:customer).permit(:name, :email, :password, :password_confirmation, :unconfirmed_email,
                                         :contact_no, :address, :district, :region,
                                         location_attributes: [:mappable_id, :mappable_type, :address, :area, :region],
                                         preferences_attributes: [:customer_id, :species_id, :breed_id, :min_age, :max_age ])

      end
    end
  end
end
