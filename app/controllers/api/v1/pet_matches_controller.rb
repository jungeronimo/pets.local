module API
  module V1
    class PetMatchesController < BaseController
      before_action :set_customer, only: [:index]

      # Get /
      def index
        @pets = @customer.matches
        respond_with(@pets)
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_customer
        @customer = ::Customer.find(params[:id])
      end

    end
  end
end
