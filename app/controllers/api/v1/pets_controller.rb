module API
  module V1
    class PetsController < BaseController
      before_action :set_pet, only: [:show]

      # POST /pets
      def create
        @pet = ::Pet.new(pet_params)
        if @pet.save
          respond_with(@pet, :status => :created, :location => api_v1_pet_path(@pet))
        else
          respond_with(@pet, status: :unprocessable_entity, template: 'shared/error')
        end
      end

      def show
        respond_with(@pet)
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_pet
        @pet = ::Pet.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def pet_params
        params.require(:pet).permit(:name, :age, :available_from, :species_id, :breed_id,
                                    location_attributes: [:mappable_id, :mappable_type, :address, :area, :region])
      end
    end

  end
end
