module API
  module V1
    class AdoptionsController < BaseController

      def create
        customer = ::Customer.find(params[:id])
        pet = ::Pet.find(params[:pet_id])

        begin
          @adoption = customer.adopt(pet)
        rescue
          respond_with({error: 'Unable to process your request.'}, status: :unprocessable_entity, template: 'shared/error')
        end

        respond_with(@adoption, :status => :created)
      end
    end
  end
end


