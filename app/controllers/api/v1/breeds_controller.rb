module API
  module V1
    class PetsController < BaseController
      before_action :set_pet, only: [:show]

      # POST /pets
      def create
        @pet = ::Pet.new(pet_params)
        if @pet.save
          respond_with(@pet.serializable_hash, :status => :created, :location => pets_path)
        else
          respond_with(@pet.errors, status: :unprocessable_entity)
        end
      end

      def show
        respond_with(@pet)
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_pet
        @pet = ::Pet.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def pet_params
        params.require(:pets).permit(:name, :age, :available_from, :species_id, :breed_id, :address, :district, :region)
      end
    end

  end
end
