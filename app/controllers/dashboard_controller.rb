class DashboardController < BaseController
  def index
    ids = current_customer.matches.collect {|m| m.id }
    @events = Event.where(observable_id: ids).limit(30)

    render '/dashboard'
  end
end
