class Admin::BaseController < ActionController::Base
  protect_from_forgery #with: :exception

  layout 'admin'

  before_filter :authorized?

  private
  def authorized?
    unless admin_signed_in? && current_admin.is_a?(::Admin)
      flash[:error] = "You must be logged in to view this page."
      redirect_to new_admin_session_path
    end
  end
end
