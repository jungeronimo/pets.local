class Admin::PetsController < Admin::BaseController
  before_action :set_admin_pet, only: [:show, :edit, :update, :destroy]

  # GET /admin/pets
  # GET /admin/pets.json
  def index
   @pets = ::Pet.includes(:location).all
  end

  # GET /admin/pets/1
  # GET /admin/pets/1.json
  def show
  end

  # GET /admin/pets/new
  def new
   @pet = ::Pet.new
  end

  # GET /admin/pets/1/edit
  def edit
  end

  # POST /admin/pets
  # POST /admin/pets.json
  def create
   @pet = ::Pet.new(admin_pet_params)

    respond_to do |format|
      if @pet.save
        format.html { redirect_to admin_pets_path, notice: 'Pet was successfully created.' }
        format.json { render :show, status: :created, location:@pet }
      else
        format.html { render :new }
        format.json { render json:@pet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/pets/1
  # PATCH/PUT /admin/pets/1.json
  def update
    respond_to do |format|
      if@pet.update(admin_pet_params)
        format.html { redirect_to [:admin, @pet], notice: 'Pet was successfully updated.' }
        format.json { render :show, status: :ok, location:@pet }
      else
        format.html { render :edit }
        format.json { render json:@pet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/pets/1
  # DELETE /admin/pets/1.json
  def destroy
   @pet.destroy
    respond_to do |format|
      format.html { redirect_to admin_pets_url, notice: 'Pet was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_pet
     @pet = ::Pet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_pet_params
      params.require(:pet).permit(:name, :age, :available_from, :species_id, :breed_id,
                                  location_attributes: [:mappable_id, :mappable_type, :address, :area, :region])
    end
end
