class Admin::CustomerPreferencesController < Admin::BaseController
  before_action :set_admin_customer_preference, only: [:show, :edit, :update, :destroy]

  # GET /admin/customer_preferences
  # GET /admin/customer_preferences.json
  def index
    @customer_preferences = ::CustomerPreference.all
  end

  # GET /admin/customer_preferences/1
  # GET /admin/customer_preferences/1.json
  def show
  end

  # GET /admin/customer_preferences/new
  def new
    @customer_preference = ::CustomerPreference.new
  end

  # GET /admin/customer_preferences/1/edit
  def edit
  end

  # POST /admin/customer_preferences
  # POST /admin/customer_preferences.json
  def create
    @customer_preference = ::CustomerPreference.new(admin_customer_preference_params)

    respond_to do |format|
      if@customer_preference.save
        format.html { redirect_to [:admin, @customer_preference], notice: 'Customer Preference was successfully created.' }
        format.json { render :show, status: :created, location:@customer_preference }
      else
        format.html { render :new }
        format.json { render json:@customer_preference.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/customer_preferences/1
  # PATCH/PUT /admin/customer_preferences/1.json
  def update
    respond_to do |format|
      if@customer_preference.update(admin_customer_preference_params)
        format.html { redirect_to [:admin, @customer_preference], notice: 'Customer Preference was successfully updated.' }
        format.json { render :show, status: :ok, location:@customer_preference }
      else
        format.html { render :edit }
        format.json { render json:@customer_preference.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/customer_preferences/1
  # DELETE /admin/customer_preferences/1.json
  def destroy
    @customer_preference.destroy
    respond_to do |format|
      format.html { redirect_to admin_customer_preferences_url, notice: 'Customer Preference was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_admin_customer_preference
    @customer_preference = ::CustomerPreference.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admin_customer_preference_params
    params.require(:customer_preference).permit(:customer_id, :species_id, :breed_id, :min_age, :max_age)
  end
end
