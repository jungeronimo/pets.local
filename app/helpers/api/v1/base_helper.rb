module API
  module V1
    module BaseHelper

      def formatted_age(age_in_months)
        years, months = age_in_months.divmod(12)
        age_in_words = ''
        if years > 0
          age_in_words += years.to_s  + 'yr'
          age_in_words += 's' if years > 1
        end

        if months > 0
          age_in_words += ' & ' unless age_in_words.blank?
          age_in_words += months.to_s + 'mo'
          age_in_words += 's' if months > 1
        end
        age_in_words
      end

    end
  end
end