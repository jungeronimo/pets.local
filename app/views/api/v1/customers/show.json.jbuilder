
json.id @customer.id
json.name @customer.name
json.email @customer.email
json.contact_no @customer.contact_no
json.location @customer.location
json.preferences @customer.preferences do |pref|
  json.species pref.species_id
  json.breed pref.breed_id
  json.min_age pref.min_age
  json.max_age pref.max_age
end


