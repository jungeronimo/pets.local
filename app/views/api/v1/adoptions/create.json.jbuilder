
json.id @adoption.id
json.customer do
  json.name @adoption.customer.name
end
json.pet do
  json.name @adoption.pet.name
end
