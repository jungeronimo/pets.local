json.species @species do |species|
  json.id species.id
  json.name species.name
  json.breeds species.breeds do |breed|
    json.id = breed.id
    json.name = breed.name
  end
end