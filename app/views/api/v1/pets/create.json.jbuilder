
json.id @pet.id
json.name @pet.name
json.age @pet.age
json.available_from @pet.available_from
json.species do
  json.id  @pet.species.id
  json.name @pet.species.name
end
json.location @pet.location