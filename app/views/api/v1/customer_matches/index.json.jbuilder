json.customers @customers do |customer|
  json.id customer.id
  json.name customer.name
  json.contact_no customer.contact_no
  json.location customer.location
end