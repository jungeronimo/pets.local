json.extract! admin_customer_preference, :id, :index, :show, :created_at, :updated_at
json.url admin_customer_preference_url(admin_customer_preference, format: :json)