json.extract! admin_pet, :id, :created_at, :updated_at
json.url admin_pet_url(admin_pet, format: :json)