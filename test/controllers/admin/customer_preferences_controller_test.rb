require 'test_helper'

class Admin::CustomerPreferencesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_customer_preference = admin_customer_preferences(:one)
  end

  test "should get index" do
    get admin_customer_preferences_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_customer_preference_url
    assert_response :success
  end

  test "should create admin_customer_preference" do
    assert_difference('Admin::CustomerPreference.count') do
      post admin_customer_preferences_url, params: { admin_customer_preference: { index: @admin_customer_preference.index, show: @admin_customer_preference.show } }
    end

    assert_redirected_to admin_customer_preference_url(Admin::CustomerPreference.last)
  end

  test "should show admin_customer_preference" do
    get admin_customer_preference_url(@admin_customer_preference)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_customer_preference_url(@admin_customer_preference)
    assert_response :success
  end

  test "should update admin_customer_preference" do
    patch admin_customer_preference_url(@admin_customer_preference), params: { admin_customer_preference: { index: @admin_customer_preference.index, show: @admin_customer_preference.show } }
    assert_redirected_to admin_customer_preference_url(@admin_customer_preference)
  end

  test "should destroy admin_customer_preference" do
    assert_difference('Admin::CustomerPreference.count', -1) do
      delete admin_customer_preference_url(@admin_customer_preference)
    end

    assert_redirected_to admin_customer_preferences_url
  end
end
