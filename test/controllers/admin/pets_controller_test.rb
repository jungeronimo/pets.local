require 'test_helper'

class Admin::PetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_pet = admin_pets(:one)
  end

  test "should get index" do
    get admin_pets_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_pet_url
    assert_response :success
  end

  test "should create admin_pet" do
    assert_difference('Admin::Pet.count') do
      post admin_pets_url, params: { admin_pet: {  } }
    end

    assert_redirected_to admin_pet_url(Admin::Pet.last)
  end

  test "should show admin_pet" do
    get admin_pet_url(@admin_pet)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_pet_url(@admin_pet)
    assert_response :success
  end

  test "should update admin_pet" do
    patch admin_pet_url(@admin_pet), params: { admin_pet: {  } }
    assert_redirected_to admin_pet_url(@admin_pet)
  end

  test "should destroy admin_pet" do
    assert_difference('Admin::Pet.count', -1) do
      delete admin_pet_url(@admin_pet)
    end

    assert_redirected_to admin_pets_url
  end
end
