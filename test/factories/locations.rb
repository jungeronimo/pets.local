FactoryGirl.define do
  factory :location do
    mappable_id 1
    mappable_type "MyString"
    address "MyString"
    area "MyString"
    region "MyString"
    latitude 1.5
    longitude 1.5
  end
end
