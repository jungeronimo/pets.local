# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160919142357) do

  create_table "admins", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  end

  create_table "adoptions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "customer_id", null: false
    t.integer  "pet_id",      null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["customer_id", "pet_id"], name: "index_adoptions_on_customer_id_and_pet_id", using: :btree
    t.index ["customer_id"], name: "index_adoptions_on_customer_id", using: :btree
    t.index ["pet_id"], name: "index_adoptions_on_pet_id", using: :btree
  end

  create_table "breeds", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "species_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customer_preferences", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "customer_id", null: false
    t.integer  "species_id"
    t.integer  "breed_id"
    t.integer  "min_age"
    t.integer  "max_age"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["breed_id"], name: "index_customer_preferences_on_breed_id", using: :btree
    t.index ["customer_id"], name: "index_customer_preferences_on_customer_id", using: :btree
    t.index ["max_age"], name: "index_customer_preferences_on_max_age", using: :btree
    t.index ["min_age"], name: "index_customer_preferences_on_min_age", using: :btree
    t.index ["species_id"], name: "index_customer_preferences_on_species_id", using: :btree
  end

  create_table "customers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "contact_no"
    t.boolean  "matched",                default: false, null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.index ["confirmation_token"], name: "index_customers_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_customers_on_email", unique: true, using: :btree
    t.index ["name"], name: "index_customers_on_name", using: :btree
    t.index ["reset_password_token"], name: "index_customers_on_reset_password_token", unique: true, using: :btree
  end

  create_table "events", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "message"
    t.integer  "observable_id"
    t.string   "observable_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "locations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "mappable_id"
    t.string   "mappable_type"
    t.string   "address"
    t.string   "area"
    t.string   "region"
    t.float    "latitude",      limit: 24
    t.float    "longitude",     limit: 24
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["latitude"], name: "index_locations_on_latitude", using: :btree
    t.index ["longitude"], name: "index_locations_on_longitude", using: :btree
    t.index ["mappable_id", "mappable_type"], name: "index_locations_on_mappable_id_and_mappable_type", using: :btree
  end

  create_table "pets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "customer_id",    default: 0,                     null: false
    t.string   "name"
    t.integer  "age"
    t.integer  "species_id",     default: 0,                     null: false
    t.integer  "breed_id",       default: 0,                     null: false
    t.datetime "available_from", default: '2016-09-19 15:54:26', null: false
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.index ["age"], name: "index_pets_on_age", using: :btree
    t.index ["available_from"], name: "index_pets_on_available_from", using: :btree
    t.index ["breed_id"], name: "index_pets_on_breed_id", using: :btree
    t.index ["customer_id"], name: "index_pets_on_customer_id", using: :btree
    t.index ["name"], name: "index_pets_on_name", using: :btree
    t.index ["species_id", "breed_id"], name: "index_pets_on_species_id_and_breed_id", using: :btree
    t.index ["species_id"], name: "index_pets_on_species_id", using: :btree
  end

  create_table "species", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
