class CreatePets < ActiveRecord::Migration[5.0]
  def change
    create_table :pets do |t|
      t.integer :customer_id, :null => false, :default => 0
      t.string :name
      t.integer :age
      t.integer :species_id, :null => false, :default => 0
      t.integer :breed_id, :null => false, :default => 0
      t.datetime :available_from, :null => false, :default => Time.now

      t.timestamps
    end

    add_index :pets, :name
    add_index :pets, :customer_id
    add_index :pets, :species_id
    add_index :pets, :breed_id
    add_index :pets, [:species_id, :breed_id]
    add_index :pets, :age
    add_index :pets, :available_from
  end
end
