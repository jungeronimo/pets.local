class CreateCustomerPreferences < ActiveRecord::Migration[5.0]
  def change
    create_table :customer_preferences do |t|
      t.integer :customer_id, :null => false
      t.integer :species_id
      t.integer :breed_id
      t.integer :min_age
      t.integer :max_age

      t.timestamps
    end

    add_index :customer_preferences, :customer_id
    add_index :customer_preferences, :species_id
    add_index :customer_preferences, :breed_id
    add_index :customer_preferences, :min_age
    add_index :customer_preferences, :max_age
  end
end
