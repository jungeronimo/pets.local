class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :message
      t.integer :observable_id
      t.string :observable_type
      t.timestamps
    end
  end
end
