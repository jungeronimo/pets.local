class CreateAdoptions < ActiveRecord::Migration[5.0]
  def change
    create_table :adoptions do |t|
      t.integer :customer_id, null: false
      t.integer :pet_id, null: false

      t.timestamps
    end

    add_index :adoptions, :customer_id
    add_index :adoptions, :pet_id
    add_index :adoptions, [:customer_id, :pet_id]
  end
end
