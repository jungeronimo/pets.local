class CreateLocations < ActiveRecord::Migration[5.0]
  def change
    create_table :locations do |t|
      t.integer :mappable_id
      t.string :mappable_type
      t.string :address
      t.string :area
      t.string :region
      t.float :latitude
      t.float :longitude

      t.timestamps
    end

    add_index :locations, [:mappable_id, :mappable_type]
    add_index :locations, :latitude
    add_index :locations, :longitude
  end
end
