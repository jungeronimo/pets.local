# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

regions = {
    'Hong Kong' => ['Sheung Wan','Midlevels West','Wan Chai','Midlevels Central','Sai Ying Pun','Causeway Bay','Kennedy Town','South Side','Central','Pokfulam','Happy Valley','Midlevels East','Peak','North Point','Tin Hau','Stanley','Quarry Bay','Admiralty','Jardines Lookout','Repulse Bay','Shau Kei Wan','Wong Chuk Hang','Fortress Hill','North Point Midlevels','Tai Hang Rd','Tai Koo Shing','Tai Tam','Ap Lei Chau','South Bay','Aberdeen','Chai Wan','Chung Hom Kok','Shouson Hill','Heng Fa Chuen','Kornhill','Red Hill','Sai Wan Ho','Shek O','Siu Sai Wan'],
    'Outlying Islands' => ['Tung Chung','Discovery Bay','South Lantau Island','Cheung Chau','Lantau Island','Ma Wan','Lamma Island','Hei Ling Chau','Mui Wo','Park Island','Peng Chau','Tai O'],
    'Kowloon' => ['Tsim Sha Tsui','Prince Edward','Mong kok','Tai Kok Tsui','Kowloon City','Ngau Chi Wan','Cheung Shan Wan','Jordan','Kowloon Bay','San Po Kong','To Kwa Wan','Yau Yat Chuen','Ho Man Tin','Kowloon Tong','Lam Tin','Sham Shui Po','Wong Tai Sin','Yau Tong','Beacon Hill','Diamond Hill','Kai Tak','Kwun Tong','Lai Chi Kok','Lok Fu','Mei Foo','Ngau Tau Kok','Shek Kip Mei','Tsz Wan Shan','Wang Tau Hom','Yau Ma Tei'],
    'New Territories' => ['Sai Kung','Clear Water Bay','Tseung Kwan O','Kwai Chung','Tai Po','Shatin','Yuen Long','Castle Peak Rd'],
    'Tuen Mun' => ['Fo Tan','Sham Tseng','Tsuen Wan','Fairview','Fanling','Fei Ngo Shan','Goldcoast','Kam Tin','Ma On Shan','Shau Tau Kok','Sheung Shui','Tai Wai','Tai Wo Hau','Tin Shui Wai','Tsing Lung Tau','Tsing Yi']
}

pet_locations = {
    'Hong Kong' =>  [
        'Doggywood Shop 3,Tung Ho Building, 177-183 Shaukeiwan Main Street East',
        'Beauty Saloon Pet Grooming, Shop B-1, G/F & M/F, 54 Electric Road, Causeway Bay',
        'Dog One Life, Shop B, G/F., 384-388 Lockhart Road, Wan Chai',
        'Dogotel & Spa Luxe, G/F, 52 Hing Fat Street, Causeway Bay',
        'Forever Pets, Room F., 3/F., Island Building, 439-445 Hennessy Road, Causeway Bay',
        'Moggy\'s Pet Shop, Unit F., 1/F., Hoi Tao Court, Gloucester Road, Causeway Bay',
        'Paw Palace, 1/F., 13 Pak Sha Road, Causeway Bay',
        'Central Animal Clinic, G/F., 100 Hing Fat Street, Causeway Bay',
        'Happy Pets Veterinary Centre, 64 Tung Lo Wan Road, Tai Hang',
        'Phoenix Animal Clinic, G/F., No. 67 Hennessy Road, Wan Chai',
        'Hong Kong Vet Centre, G/F., 12-12A Kennedy Street, Wan Chai',
        'SPCA - Hong Kong Centre, 5 Wan Shing Street, Wanchai',
        'Furball, G/F., 22 Po Hing Fong, Sheung Wan',
        'Puppy Doggy, Shop 2, G/F., Kennedy Town Building, 27 Kennedy Town Praya',
        'Red Red Dog, Shop 1A, M/F., Southern Pearl Court, 151-153, Wong Nai Chung Road, Happy Valley',
        'Dr. Eric\'s Animal Clinic, Portion B, G/F. Igloo Residence, 1A Shan Kwong Road, Happy Valley',
        'Pet & Co. 專業寵物美容中心, Shop B1, G/F., 100-104 Sai Wan Ho Street, San Wan Ho',
        'Pets Paradise, Shop154, Fullview Arcade, 18 Siu Sai Wan Rd., Chai Wan',
        'Dogaroo Dog @ Stanley, Unit 404-405, 4/F., Stanley Plaza, Carmel Road, Stanley',
        'Pacific Pets, G/F 10 Wong Ma Kok Rd., Stanley',
        'Stanley Vet Station, B/F 10 Wong Ma Kok Rd., Stanley',
        'Little Princess, Shop 6, 1-2 St. Stephen\'s Lane, Mid-levels West',
        'Doggeteer, 20 Mosque Junction, Mid-Levels',
        'Mosque Street Pet Services, G/F, Shop No.4, 31-37 Mosque Street, Mid Levels',
        'Supet Market, Shop B12, B/F., Jumbo Building, 201 Aberdeen Main Road, Aberdeen',
        'Whiskers N Paws, Room 1013, 10/F., Horizon Plaza, 2 Lee Wing Street, Ap Lei Chau'
    ],

    'Kowloon' => [
        'Pet Pet Organic, Rm1013, 10/F, East Wing, Tsim Sha Tsui Ctr, 66 Mody Rd, Tsim Sha Tsui',
        'Dog.com, G/F., 77 Tam Kung Road, To Kwa Wan',
        'Dogaroo Dog Training Center, G/F., 1B Liberty Avenue, Mongkok',
        'ogaroo @ Kitty Doggy, G/F., 188 Tung Choi Street, Mongkok',
        'Dogotel & Spa, Shop 14, G/F, 124E Argyle Street, Mongkok',
        'Kitty\'s Family, 4A, Tat Lee Building, 43 Dundas Street, Mong Kok',
        'Doggie Taste, Unit E2, 6/F, Yip Win Factory Building, 10 Tsun Yip Lane, Kwun Tong',
        'Pets Eleven Pet Shop, Shop 2, G/F., Yee On Centre, 45 Hong Ning Road, Kwun Tong',
        'Sofu Dog, M/F., 140 Shui Wo Street, Kwun Tong, Kowloon, Hong Kong',
        'Doggy Buddy, Shop A, G/F., 51 Tai Tsun Street, Tai Kok Tsui',
        'Miko Pet Shop, Shop 13, G/F., Metro Harbourview Plaza, 8 Fuk Lee Street, Tai Kok Tsui',
        'Paws and Claws, G/F., 8 Chung Wui Street, Tai Kok Tsui',
        'Dream Works, Shop 1D, G/F., Banyan Mall, 863 Lai Chi Kok Road, Cheung Sha Wan',
        'Pet Dog Grooming House, Unit 2307, 23/F., Peninsula Tower, 538 Castle Peak Road, Lai Chi Kok',
        'Sun Lucky Pet Shop, G/F., 136 Castle Peak Road, Cheung Sha Wan',
        'Golden Lucky Pet Shop, G/F., 52 Un Chau Street, Sham Shui Po',
        'Just Care, Shop G32, Mikiki Mall, 638, Prince Edward Road East, San Po Kong',
        'I Love Pet Pet, Shop B, G/F., To Kwa Wan Road, To Kwa Wan',
        'Penthouse Dog, M/F., 85 Lion Rock Road, Kowloon City',
        'SilverRock Pet Shop, Shop A, G/F., Homantin Mansion, Ho Man Tin Street',
        'Centaur Veterinary Clinic, G/F., 30A Victory Ave, Ho Man Tin',
        'SPCA - Kowloon Centre, 105 Princess Margaret Road'
    ],

    'New Territories' => [
        'Blue Strawberry, Shop A, G/F., Sha Tsui Road, Tsuen Wan',
        'WisePet, P. O. Box 804, Tsuen Wan Post Office ',
        'Dog Z Factory, Unit 1, 1/F., Lucida Industrial Building, Wang Lung Street, Tsuen Wan',
        'Forever Pets, Room 14, 1/F., Cheuk Ming Mansion, 57-75 Tai Ho Road, Tsuen Wan ',
        '寵物小天地, G/F, 35 Tsing Fai San Tsuen, Tsing Yi',
        'Dog Village Pet Shop, G/F., 72 Sun Tin Village, Chui Tin Street, Tai Wai, Shatin',
        'Promise Forever, Shop E1, G/F., 4 Chik Fuk Street, Tai Wai',
        'Tai Wai Small Animal & Exotic Hospital, 75 Chik Shun Street, Tai Wai, Shatin',
        'Pet Go Go Go, Shop 2125, 2/F., Ma On Shan Plaza, 608 Sai Sha Road, Shatin',
        '狗狗鎮寵物美容屋, Shop 6, G/F., Bamboo Court, Castle Peak Road – San Hui, Tuen Mun',
        'Momo Pet Shop, Shop 3, Aegean Coast Shopping Arcade, 2 Kwun Tsing Road, Tuen Mun',
        '屯門寵物店, Shop 2, G/F., Lucky Building, 117 Castle Peak Road (san Hui), Tuen Mun',
        'Tuen Mun Public Riding School, Lot 45 Lung Mun Road, Tuen Mun',
        'Pet Shop Boys, G/F., 15 Ho Shun King Building, 3 Fung Yau Street South, Yuen Long',
        'King Wai Pet Shop, G/F., Block G, Fung Yue Building, 72 Kau Yuk Road, Yuen Long',
        'Pet King Pet Shop, G/F., 19 Yau San Street, Yuen Long',
        'Beas River Country Club, Sheung Shui',
        'I Love U Dogz, Shop E2, G/F., Ocean Shores, Phase 3, Tseung Kwan O',
        'Clearwater Bay Equestrian & Education Centre, DD 285, Lots 139-144, Clearwater Bay',
        'SPCA - Sai Kung Centre, No.7, Sha Tsui Path, Sai Kung'
    ],

    'Islands' => [
        'Pet Stop, Shop B2, Caribbean Bazaar, Tung Chung, Lantau Island, Hong Kong',
        'Pets Gallery, Shop G11B, Discovery Bay Plaza, Discovery Bay, Lantau Island, Hong Kong'
    ]
}

customer_locations = {
    'Hong Kong' => [
        '2/F., Winner House,15 Wong Nei Chung Road, Happy Valley',
        '10/F, Iuki Tower, 5-7 O\'Brien Road, Wanchai',
        '11/F., New Spring Gdn Mansion, 47-56 Spring Garden Lane, Wanchai',
        '2/F, Harbour Centre, 25 Harbour Rd., Wanchai',
        'G/F, Professional Bldg., 19-23 Tung Lo Wan Road',
        '20 Luard Road, Wanchai',
        '13/F, 151 Lockhart Road, Wanchai',
        '12-18 Morrison Hill Rd',
        '168-174 Tung Lo Wan Rd, Causeway Bay',
        '2/F Siu Fung Building, 9-17 Tin Lok Lane, Wanchai',
        '16/F, Hennessy House (CLI Bldg), 313-317B Hennesy Rd, Wanchai',
        '3/F, Allied Kajima Bldg., 138 Gloucester Road, Wanchai',
        '5/F., Kam Kwong Mansion, 36-44 King Kwong St, Happy Valley',
        'G/F, The Chinese Bank Bldg, 2A Pottinger St, 61-65 Des Voeux Rd C',
        '23/F, Grand View Comm Bldg, Nos.29-31 Sugar St, Causeway Bay',
        'No. 16 Matheson Street, Causeway Bay',
        '1 Jubilee St., Central',
        '8-12 D\'Aguilar Street, Central',
        '78 Hennessy Road, Wanchai',
        'Jade House, 11-15 Fleming Rd., Wanchai',
        'No.61-73 Lee Garden Rd., Causeway Bay',
        'No.39E, 39F & 39G Sing Woo Rd and No.6 Yuen Yuen St., Happy Valley',
        'Malahon Apartments, Nos.513 Jaffe Rd., Causeway Bay',
        'No.64 Lockhart Road, Wanchai',
        '99 Hennessy Road, Wanchai',
        'Great Eagle Centre, 23 Harbour Road, Wanchai',
        '7 Gloucester Road, Wanchai',
        'G/F., Lei Shun Court, No.106-126 Leighton Road, Causeway Bay',
    ],
    'Kowloon' => [
        '35-37 To Kwa Wan Road',
        '31C Chi Kiang Street',
        'No.46-48 Ma Tau Wai Rd, Hung Hom',
        '8 Peking Road, Tsim Sha Tsui',
        '28/F, Hotel Icon, 17 Science Museum Road',
        '3/F Elements, 1 Austin Road West',
        '132-134 Nathan Rd',
        'B/F, Star House, 3 Salisbury Road, Tsim Sha Tsui',
        '688 Nathan Road, Mong Kok',
        '114B Broadway St, Mei Foo Sun Chuen Stage 8, Mei Foo',
        'G/F, Wei Ming Centre, No. 53 Hung To Road, Kwun Tong',
        '2/F, Gateway Arcade, Harbour City, 17 Canton Road, Tsim Sha Tsui',
        '31-37 Jordan Road, Jordan',
        '2/F, 11 Hong Ning Rd, Kwun Tong',
        '1/F, Lee Hong Hse, Shun Lee Est, Kwun Tong',
        '8 Hang Cheung Street, Cheung Sha Wan',
        '4-5,1/F,China Hong Kong City Bik,33 Canton Road, Tsim Sha Tsui',
        '80 Tat Chee Avenue, Kowloon Tong',
        '388 Kwun Tong Road, Kwun Tong'
    ],
    'New Territories' => [
        '2/F, Nan Fung Centre, 264 Castle Peak Road (Tsuen Wan), Tsuen Wan',
        '3/F, Nan Fung Plaza, 8 Pui Shing Road, Hang Hau, Tseung Kwan O',
        '4/F, Kwai Chung Plaza, 7 Kwai Foo Road, Kwai Fong',
        '4/F, Phase 1, New Town Plaza, 18 Sha Tin Centre Street, Sha Tin',
        '2/F, Yin Lai Crt Shopping Centre, 180 Lai King Hill Road, Kwai Chung',
        'Level 2, K-Point, 1 Tuen Lung Street, Tuen Mun',
        '10/F Yiu On Centre, Yiu On Est, Ma On Shan',
        '8/F, 140 San Fung Avenue, Sheung Shui',
        '12/F, Maritime Square, 33 Tsing King Road, Tsing Yi',
        '2/F The Palazzo Shopping Centre, 28 Lok King Street, Fo Tan',
        '2/F, Leung King Commercial Complex, 31 Tin King Road, Tuen Mun',
        '23 Sha On Street, Ma On Shan',
        '2/F, Discovery Park, 398 Castle Peak Road (Tsuen Wan), Tsuen Wan',
        '2/F, Choi Ming Shopping Centre, 1 Choi Ming Street, Tiu Keng Leng, Tseung Kwan O',
        '3/F, Tin Chak Shopping Centre, Tin Shui Wai',
        '1 Yeung Uk Road, Tsuen Wan'
    ]
}



dog_names = ['Bella','Buddy','Molly','Max','Bailey','Charlie','Daisy','Lucy','Sadie','Chloe','Maggie','Rocky','Roxy','Jack','Lola','Peanut','Toby','Marley','Coco','Gizmo','Lucky','Bear','Cooper','Harley','Jake','MIlo','Sophie','Lily','Riley','Shadow','Ginger','Mia','Ruby','ANGEL','Buster','Pepper','Zoey','Duke','Bruno','Lilly','Oliver','Zoe','Bandit','Cookie','Dexter','Princess','Teddy','Penny','Snickers','Stella','Abby','Bentley','Emma','Gracie','Lexi','Sam','Missy','Oreo','Rosie','Baby','Lulu','luna','Sasha','Baxter','CODY','Rocco','Tucker','Ellie','Mocha','Casey','Copper','Izzy','Koda','Rusty','Scooter','Bo','Bubba','Heidi','Spike','Belle','Cleo','Joey','Rudy','Sammy','dixie','Holly','Jasper','Lady','Leo','Romeo','Roxie','Sassy','sugar','Tyson','Jackson','Katie','Layla','Precious','Rex','CASSIE']
cat_names = ['Bella', 'Tigger', 'Chloe', 'Shadow', 'Oliver', 'Kitty', 'Lucy', 'Molly', 'Jasper', 'Luna', 'Oreo', 'Smokey', 'Gizmo', 'Simba', 'Charlie', 'Tiger', 'Angel', 'Jack', 'Lily', 'Peanut', 'Toby', 'Baby', 'Loki', 'Midnight', 'Milo', 'Princess', 'Sophie', 'Harley', 'Max', 'Missy', 'Rocky', 'Zoe', 'CoCo', 'Nala', 'Oscar', 'Pepper', 'Buddy', 'Pumpkin', 'Sasha', 'Kiki', 'mittens', 'bailey', 'Callie', 'Lucky', 'Misty', 'Patches', 'Simon', 'Garfield', 'George', 'Maggie', 'Sebastian', 'Boots', 'Cali', 'Felix', 'Lilly', 'Sammy', 'Sassy', 'tucker', 'bandit', 'Dexter', 'Jake', 'Phoebe', 'Precious', 'Romeo', 'Snickers', 'Socks', 'Daisy', 'Fiona', 'Lola', 'Sadie', 'sox', 'Casper', 'Fluffy', 'Gracie', 'Marley', 'minnie', 'Sweetie', 'Ziggy', 'Belle', 'Blackie', 'Chester', 'Frankie', 'Ginger', 'Muffin', 'Murphy', 'Rusty', 'Scooter', 'BatMan', 'boo', 'Cleo', 'Izzy', 'Jasmine', 'MIMI', 'SUGAR', 'cupcake', 'Dusty', 'Leo', 'Noodle', 'Panda', 'Salem']

puts 'Creating admin user admin@pets.local with password: "password"'
Admin.create(email: 'admin@pets.local', password: 'password')

puts 'Creating Cat species and breeds'
cat = Species.create(:name => 'Cat')
cat_breeds = ['Abyssinian', 'American Bobtail','American Curl','American Shorthair','American Wirehair','Balinese','Bengal Cats','Birman','Bombay','British Shorthair','Burmese','Burmilla','Chartreux','Chinese Li Hua','Colorpoint Shorthair','Cornish Rex','Cymric','Devon Rex','Egyptian Mau','European Burmese','Exotic','Havana Brown','Himalayan','Japanese Bobtail','Javanese','Korat','LaPerm','Maine Coon','Manx','Nebelung','Norwegian Forest','Ocicat','Oriental','Persian','Ragamuffin','Ragdoll Cats','Russian Blue','Savannah','Scottish Fold','Selkirk Rex','Siamese Cat','Siberian','Singapura','Snowshoe','Somali','Sphynx','Tonkinese','Turkish Angora','Turkish Van']
cat.breeds.create(cat_breeds.collect{|b| {:name => b} })

puts 'Creating Dog species and breeds'
dog = Species.create(:name => 'Dog')
dog_breeds = ['Affenpinscher','Afghan Hound','Airedale Terrier','Akita','Alaskan Malamute','American Cocker Spaniel','American Eskimo Dog (Miniature)','American Eskimo Dog (Standard)','American Eskimo Dog (Toy)','American Foxhound','American Staffordshire Terrier','American Water Spaniel','Anatolian Shepherd','Australian Cattle Dog','Australian Shepherd','Australian Terrier','Basenji','Basset Hound','Beagle','Bearded Collie','Bedlington Terrier','Belgian Malinois','Belgian Sheepdog','Belgian Tervuren','Bernese Mountain Dog','Bichon Frise','Black and Tan Coonhound','Bloodhound','Border Collie','Border Terrier','Borzoi','Boston Terrier','Bouvier des Flandres','Boxer','Briard','Brittany','Brussels Griffon','Bull Terrier','Bulldog','Bullmastiff','Cairn Terrier','Canaan Dog','Cardigan Welsh Corgi','Cavalier King Charles Spaniel','Chesapeake Bay Retriever','Chihuahua','Chinese Crested Dog','Chinese Shar-Pei','Chow Chow','Clumber Spaniel','Collie','Curly-Coated Retriever','Dachshund (Standard)','Dachsund (Miniature)','Dalmatian','Dandie Dinmont Terrier','Doberman Pinscher','English Cocker Spaniel','English Foxhound','English Setter','English Springer Spaniel','English Toy Spaniel','Field Spaniel','Finnish Spitz','Flat-Coated Retriever','French Bulldog','German Shepherd Dog','German Shorthaired Pointer','German Wirehaired Pointer','Giant Schnauzer','Golden Retriever','Gordon Setter','Great Dane','Great Pyrenees','Greater Swiss Mountain Dog','Greyhound','Harrier','Havanese','Ibizan Hound','Irish Setter','Irish Terrier','Irish Water Spaniel','Irish Wolfhound','Italian Greyhound','Jack Russell Terrier','Japanese Chin','Keeshond','Kerry Blue Terrier','Komondor','Kuvasz','Labrador Retriever','Lakeland Terrier','Lhasa Apso','Lowchen','Maltese','Manchester Terrier (Standard)','Manchester Terrier (Toy)','Mastiff','Miniature Bull Terrier','Miniature Pinscher','Miniature Schnauzer','Newfoundland','Norfolk Terrier','Norwegian Elkhound','Norwich Terrier','Old English Sheepdog','Otterhound','Papillon','Pekingese','Pembroke Welsh Corgi','Petit Basset Griffon Vendeen','Pharaoh Hound','Pointer','Pomeranian','Poodle (Miniature)','Poodle (Standard)','Poodle (Toy)','Portuguese Water Dog','Pug','Puli','Rhodesian Ridgeback','Rottweiler','Saint Bernard','Saluki (or Gazelle Hound)','Samoyed','Scottish Deerhound','Scottish Terrier','Sealyham Terrier','Shetland Sheepdog','Shiba Inu','Shih Tzu','Siberian Husky','Silky Terrier','Skye Terrier','Smooth Fox Terrier','Soft Coated Wheaten Terrier','Spinone Italiano','Staffordshire Bull Terrier','Standard Schnauzer','Sussex Spaniel','Tibetan Spaniel','Tibetan Terrier','Vizsla','Weimaraner','Welsh Springer Spaniel','Welsh Terrier','West Highland White Terrier','Whippet','Wire Fox Terrier','Wirehaired Pointing Griffon','Yorkshire Terrier']
dog.breeds.create(dog_breeds.collect{|b| {:name => b} })

puts 'Seeding pets. Using geocoding for location. It might take a while...'
300.times {
  species = [cat, dog].sample
  breed = species.breeds.to_a.sample
  name = species.name == 'Cat' ? cat_names.sample : dog_names.sample
  age = (1..60).to_a.sample
  available_from = Faker::Date.between(120.days.ago, Date.today)
  region = ['Hong Kong','Kowloon', 'New Territories', 'Islands'].sample
  location = Location.new(address: pet_locations[region].sample, region: region)

  Pet.create(
      name: name,
      species: species,
      breed: breed,
      age: age,
      available_from: available_from,
      location: location
  );
}

puts 'Seeding customers with default password: "password". Using geocoding for location. It might take a while...'
3.times {
  regions = ['Hong Kong','Kowloon', 'New Territories']
  regions.each do |reg|
    customer_locations[reg].each do |addr|

      location = Location.new(address: addr, region: reg)

      customer = Customer.new(
          name: Faker::Name.name,
          email: Faker::Internet.email,
          contact_no: (['3','5','9'].sample + Faker::PhoneNumber.subscriber_number(7)),
          password: 'password',
          password_confirmation: 'password',
          confirmed_at: Time.now,
          location: location
      );

      rand = (1..100).to_a.sample

      # species = nil
      # breed = nil
      #if rand > 5
        species = [cat, dog].sample
      #  if rand > 60
          breed = species.breeds.to_a.sample
      #  end
      #end

      if rand > 80
        min_age = (3..24).to_a.sample
        max_age = (min_age..60).to_a.sample
      else
        min_age = nil
        max_age = nil
      end

      customer.preferences.build(
          species: species,
          breed: breed,
          min_age: min_age,
          max_age: max_age
      )
      customer.save!
    end
  end
}
