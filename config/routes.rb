Rails.application.routes.draw do

  constraints subdomain: 'api' do
    namespace :api, path: '/' do
      namespace :v1, path: '/v1', defaults: { :format => 'json' } do
        resources :customers, only: [:show, :create] do
          member do
            get 'matches', to: 'pet_matches#index'
            post 'adopt', to: 'adoptions#create'
          end
        end

        resources :pets, only: [:show, :create] do
          member do
            get 'matches', to: 'customer_matches#index'
          end
        end

        resources :species
      end
    end
  end

  mount ActionCable.server => '/cable'

  devise_for :admins, path: 'admin',
             path_names: {sign_in: 'sign-in', sign_out: 'sign-out'},
             controllers: { sessions: "admin/sessions" }

  devise_for :customers, path: '',
             path_names: {sign_in: 'sign-in', sign_out: 'sign-out'},
             controllers: { sessions: "sessions" }

  get '/', to: 'dashboard#index', as: 'dashboard'
  resources :pets, only: [:index]
  resources :customer_preferences


  namespace :admin do
    get '/', to: 'dashboard#index', as: 'dashboard'
    resources :admins, only: [:index, :show]

    resources :customer_preferences
    resources :customers
    resources :pets
  end

  resources :events
end